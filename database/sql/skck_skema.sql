-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 07 Feb 2020 pada 08.51
-- Versi server: 10.4.10-MariaDB
-- Versi PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skck_skema`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `kabupatenkota`
--

CREATE TABLE `kabupatenkota` (
  `id` int(11) NOT NULL,
  `provinsi_id` int(11) NOT NULL,
  `namawilayah` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `kabupatenkota`
--

INSERT INTO `kabupatenkota` (`id`, `provinsi_id`, `namawilayah`) VALUES
(1, 1, 'KABUPATEN BANDUNG'),
(2, 1, 'KOTA BANDUNG'),
(3, 2, 'KOTA PASIFIK'),
(4, 2, 'KABUPATEN PASIFIK');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kecamatan`
--

CREATE TABLE `kecamatan` (
  `id` int(11) NOT NULL,
  `kabupatenkota_id` int(11) NOT NULL,
  `namakecamatan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `kecamatan`
--

INSERT INTO `kecamatan` (`id`, `kabupatenkota_id`, `namakecamatan`) VALUES
(1, 1, 'SOREANG'),
(2, 1, 'CIWIDEY'),
(3, 2, 'ARJASARI'),
(4, 2, 'SOEKARNO HATTA'),
(5, 3, 'ANDAMAN'),
(6, 3, 'ANDAMAN TENGAH'),
(7, 4, 'LAUT'),
(8, 4, 'LAUT TENGAH');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelurahan`
--

CREATE TABLE `kelurahan` (
  `id` int(11) NOT NULL,
  `kecamatan_id` int(11) NOT NULL,
  `nama_kelurahan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `kelurahan`
--

INSERT INTO `kelurahan` (`id`, `kecamatan_id`, `nama_kelurahan`) VALUES
(1, 1, 'CINGCIN'),
(2, 1, 'KATAPANG'),
(3, 2, 'RANCABALI'),
(4, 2, 'RANCABATU'),
(5, 3, 'KEBON KALAPA'),
(6, 3, 'KEBON BATU'),
(7, 4, 'SOETTA'),
(8, 4, 'LEUWI PANJANG'),
(9, 5, 'BATU ANDAMAN'),
(10, 5, 'KORAL ANDAMAN'),
(11, 6, 'IKAN ANDAMAN TENGAH'),
(12, 6, 'CUMI ANDAMAN TENGAH'),
(13, 7, 'TELUK LAUT'),
(14, 7, 'JURANG LAUT'),
(15, 8, 'MARIANA LAUT TENGAH'),
(16, 8, 'TEPI LAUT TENGAH');

-- --------------------------------------------------------

--
-- Struktur dari tabel `polsek`
--

CREATE TABLE `polsek` (
  `id` int(11) NOT NULL,
  `provinsi_id` int(11) NOT NULL,
  `nama_polda` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `polsek`
--

INSERT INTO `polsek` (`id`, `provinsi_id`, `nama_polda`) VALUES
(1, 1, 'POLDA JAWA BARAT'),
(2, 2, 'POLDA JAWA SELATAN');

-- --------------------------------------------------------

--
-- Struktur dari tabel `provinsi`
--

CREATE TABLE `provinsi` (
  `id` int(11) NOT NULL,
  `nama_provinsi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `provinsi`
--

INSERT INTO `provinsi` (`id`, `nama_provinsi`) VALUES
(1, 'JAWA BARAT'),
(2, 'JAWA SELATAN');

-- --------------------------------------------------------

--
-- Struktur dari tabel `satwil`
--

CREATE TABLE `satwil` (
  `id` int(11) NOT NULL,
  `provinsi_id` int(11) NOT NULL,
  `nama_satwil` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `satwil`
--

INSERT INTO `satwil` (`id`, `provinsi_id`, `nama_satwil`) VALUES
(1, 1, 'POLDA JAWA BARAT'),
(2, 2, 'POLDA JAWA SELATAN');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `kabupatenkota`
--
ALTER TABLE `kabupatenkota`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kelurahan`
--
ALTER TABLE `kelurahan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `polsek`
--
ALTER TABLE `polsek`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `provinsi`
--
ALTER TABLE `provinsi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `satwil`
--
ALTER TABLE `satwil`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `kabupatenkota`
--
ALTER TABLE `kabupatenkota`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `kecamatan`
--
ALTER TABLE `kecamatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `kelurahan`
--
ALTER TABLE `kelurahan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `polsek`
--
ALTER TABLE `polsek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `provinsi`
--
ALTER TABLE `provinsi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `satwil`
--
ALTER TABLE `satwil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
