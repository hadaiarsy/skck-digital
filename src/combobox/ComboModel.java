/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package combobox;

/**
 *
 * @author spica
 */
public class ComboModel {
    private String id;
    private String nama;
    private ComboListener comboListener;

    public ComboListener getComboListener() {
        return comboListener;
    }

    public void setComboListener(ComboListener comboListener) {
        this.comboListener = comboListener;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
        fireOnChange();
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
        fireOnChange();
    }
    
    private void fireOnChange() {
        if (comboListener != null) {
            comboListener.onChange(this);
        }
    }
}
