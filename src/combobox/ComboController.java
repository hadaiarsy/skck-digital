/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package combobox;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author spica
 */
public class ComboController {
    private ComboModel model;
    private ComboView view;
    
    public void setModel(ComboModel model){
        this.model = model;
    }
    
    public void setCbProvinsi(ComboView view){
        try {
            String getProvinsi = "Select * From provinsi";
            Connection con = Config.configDB();
            Statement state = con.createStatement();
            ResultSet rs = state.executeQuery(getProvinsi);
            while(rs.next()){
                Object[] ob = new Object[1];
                ob[0] = rs.getString(2);

                view.getCbProvinsi().addItem(ob[0]);                                      
            }
            rs.close(); 
            state.close();
        } catch (SQLException ex) {
            Logger.getLogger(ComboController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void clearCbKota(ComboView view){
        view.getCbKota().removeAllItems();
    }
    
    public void setCbKota(ComboView view){
        try {
            Connection con = Config.configDB();
            Statement state = con.createStatement();
            
            String setProvinsi = view.getCbProvinsi().getSelectedItem().toString();
            String getIdProvinsi = "Select * from provinsi where nama = '"+setProvinsi+"'";
            ResultSet rsi = state.executeQuery(getIdProvinsi);
            
            while (rsi.next()) {
               String idProvinsi = rsi.getString("id");
               view.getJlableCoba().setText(idProvinsi);
               String getKota = "Select * From kota where provinsi_id = '"+idProvinsi+"'";
               ResultSet rs = state.executeQuery(getKota);
               while(rs.next()){
                   Object[] ob = new Object[1];
                   ob[0] = rs.getString(3);

                   view.getCbKota().addItem(ob[0]);                                     
               }
               rs.close(); 
               state.close();
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(ComboController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void setCbKecamatan(ComboView view){
        try {
            String setKecamatan = view.getCbKecamatan().getSelectedItem().toString();
            String getKecamatan = "Select * From kecamatan";
            Connection con = Config.configDB();
            Statement state = con.createStatement();
            ResultSet rs = state.executeQuery(getKecamatan);
            while(rs.next()){
                Object[] ob = new Object[1];
                ob[0] = rs.getString(3);

                view.getCbKecamatan().addItem(ob[0]);                                     
            }
            rs.close(); 
            state.close();
        } catch (SQLException ex) {
            Logger.getLogger(ComboController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
