/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package development;

/**
 *
 * @author spica
 */
public class SkckModel {
    private String jenis_keperluan,kesatuan_wilayah,alamat_ktp,provinsi,kota,kecamatan,kelurahan,cara_bayar;
    private SkckListener skckListener;
    
    public String getProvinsi() {
        return provinsi;
    }

    public void setProvinsi(String provinsi) {
        this.provinsi = provinsi;
        fireOnChange();
    }
    
    public SkckListener getSkckListener() {
        return skckListener;
    }

    public void setSkckListener(SkckListener skckListener) {
        this.skckListener = skckListener;
    }

    public String getKesatuan_wilayah() {
        return kesatuan_wilayah;
    }

    public void setKesatuan_wilayah(String kesatuan_wilayah) {
        this.kesatuan_wilayah = kesatuan_wilayah;
        fireOnChange();
    }

    public String getAlamat_ktp() {
        return alamat_ktp;
    }

    public void setAlamat_ktp(String alamat_ktp) {
        this.alamat_ktp = alamat_ktp;
        fireOnChange();
    }

    public String getKota() {
        return kota;
    }

    public void setKota(String kota) {
        this.kota = kota;
        fireOnChange();
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
        fireOnChange();
    }

    public String getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
        fireOnChange();
    }

    public String getCara_bayar() {
        return cara_bayar;
    }

    public void setCara_bayar(String cara_bayar) {
        this.cara_bayar = cara_bayar;
        fireOnChange();
    }

    public String getJenis_keperluan() {
        return jenis_keperluan;
    }

    public void setJenis_keperluan(String jenis_keperluan) {
        this.jenis_keperluan = jenis_keperluan;
        fireOnChange();
    }
    
    private void fireOnChange() {
        if (skckListener != null) {
            skckListener.onChange(this);
        }
    }
}
