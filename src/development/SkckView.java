/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package development;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

/**
 *
 * @author spica
 */
public class SkckView extends javax.swing.JFrame implements SkckListener{

    private SkckController controller;
    private SkckModel model;
    
    public SkckView() {
        
        controller = new SkckController();
        model = new SkckModel();
        model.setSkckListener(this);
        controller.setModel(model);
        initComponents();
        
        controller.setKeperluan(this);
    }

    public JComboBox getCbKeperluan(){
        return CbKeperluan;
    }
    
    public JComboBox getCbSatwil(){
        return CbSatwil;
    }
    
    public JComboBox getCbProvinsi(){
        return CbProvinsi;
    }
    
    public JComboBox getCbKota(){
        return CbKota;
    }
    
    public JComboBox getCbKecamatan(){
        return CbKecamatan;
    }
    
    public JComboBox getCbKelurahan(){
        return CbKelurahan;
    }
    
    public JTextField getTxtAlamat(){
        return TxtAlamat;
    }
    
    public JTextField getTxtBriva(){
        return TxtBriva;
    }
    
    public JRadioButton getRbTunai(){
        return RbTunai;
    }
    
    public JRadioButton getRb(){
        return RbBriva;
    }
    
    public JLabel getTestLabel(){
        return TestLabel;
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        CbKeperluan = new javax.swing.JComboBox<>();
        CbSatwil = new javax.swing.JComboBox<>();
        CbKota = new javax.swing.JComboBox<>();
        CbKecamatan = new javax.swing.JComboBox<>();
        CbKelurahan = new javax.swing.JComboBox<>();
        TxtAlamat = new javax.swing.JTextField();
        RbTunai = new javax.swing.JRadioButton();
        RbBriva = new javax.swing.JRadioButton();
        TxtBriva = new javax.swing.JTextField();
        CbProvinsi = new javax.swing.JComboBox<>();
        TestLabel = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        CbKeperluan.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        CbKeperluan.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                CbKeperluanMouseClicked(evt);
            }
        });

        CbSatwil.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        CbSatwil.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                CbSatwilMouseClicked(evt);
            }
        });

        CbKota.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        CbKota.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                CbKotaMouseClicked(evt);
            }
        });

        CbKecamatan.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        CbKecamatan.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                CbKecamatanMouseClicked(evt);
            }
        });

        CbKelurahan.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        CbKelurahan.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                CbKelurahanMouseClicked(evt);
            }
        });

        buttonGroup1.add(RbTunai);
        RbTunai.setText("Tunai");

        buttonGroup1.add(RbBriva);
        RbBriva.setText("BRIVA");
        RbBriva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RbBrivaActionPerformed(evt);
            }
        });

        CbProvinsi.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        CbProvinsi.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                CbProvinsiMouseClicked(evt);
            }
        });

        TestLabel.setText("jLabel1");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(CbKeperluan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(CbSatwil, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(CbKota, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CbKecamatan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CbKelurahan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(TxtAlamat, javax.swing.GroupLayout.PREFERRED_SIZE, 371, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CbProvinsi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(TestLabel)
                            .addComponent(RbTunai))
                        .addGap(18, 18, 18)
                        .addComponent(RbBriva)
                        .addGap(18, 18, 18)
                        .addComponent(TxtBriva, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(19, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CbKeperluan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CbSatwil, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(TxtAlamat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(CbProvinsi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9)
                .addComponent(CbKota, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(CbKecamatan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(CbKelurahan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(RbTunai)
                    .addComponent(RbBriva)
                    .addComponent(TxtBriva, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addComponent(TestLabel)
                .addContainerGap(34, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void RbBrivaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RbBrivaActionPerformed
        //TestLabel.setText(Rb.Briva.getText().toString());
//        String tes = CbProvinsi.getSelectedItem().toString();
//        TestLabel.setText(tes);

    }//GEN-LAST:event_RbBrivaActionPerformed

    private void CbKeperluanMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CbKeperluanMouseClicked
        controller.setSatwil(this);
    }//GEN-LAST:event_CbKeperluanMouseClicked

    private void CbSatwilMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CbSatwilMouseClicked
        controller.getProvinsiId(this);
        controller.setProvinsi(this);
    }//GEN-LAST:event_CbSatwilMouseClicked

    private void CbProvinsiMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CbProvinsiMouseClicked
        controller.getKotaId(this);
        controller.setKota(this);
    }//GEN-LAST:event_CbProvinsiMouseClicked

    private void CbKotaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CbKotaMouseClicked
        controller.getKecamatanId(this);
        controller.setKecamatan(this);
    }//GEN-LAST:event_CbKotaMouseClicked

    private void CbKecamatanMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CbKecamatanMouseClicked
        controller.getKelurahanId(this);
        controller.setKelurahan(this);
    }//GEN-LAST:event_CbKecamatanMouseClicked

    private void CbKelurahanMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CbKelurahanMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_CbKelurahanMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(SkckView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(SkckView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(SkckView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(SkckView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new SkckView().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> CbKecamatan;
    private javax.swing.JComboBox<String> CbKelurahan;
    private javax.swing.JComboBox<String> CbKeperluan;
    private javax.swing.JComboBox<String> CbKota;
    private javax.swing.JComboBox<String> CbProvinsi;
    private javax.swing.JComboBox<String> CbSatwil;
    private javax.swing.JRadioButton RbBriva;
    private javax.swing.JRadioButton RbTunai;
    private javax.swing.JLabel TestLabel;
    private javax.swing.JTextField TxtAlamat;
    private javax.swing.JTextField TxtBriva;
    private javax.swing.ButtonGroup buttonGroup1;
    // End of variables declaration//GEN-END:variables

    @Override
    public void onChange(SkckModel pelanggan) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
