/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package development;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author spica
 */
public class SkckController {
    private SkckModel model;
    private SkckView view;
    String idProvinsi;
    String idKota;
    String idKecamatan;
    String idKelurahan;
    
    public void setModel(SkckModel model){
        this.model = model;
    }
    
    public void setTextTest(SkckView view){
        //Buat TESSSSS 
    }
    
    public void setKeperluan(SkckView view){
        view.getCbKeperluan().removeAllItems();
        view.getCbKeperluan().addItem("Melamar Pekerjaan");
    }
    
    public void setSatwil(SkckView view){
        view.getCbSatwil().removeAllItems();
        try {
            String getProvinsi = "Select * From satwil";
            Connection con = Config.configDB();
            Statement state = con.createStatement();
            ResultSet rs = state.executeQuery(getProvinsi);
            while(rs.next()){
                Object[] ob = new Object[1];
                ob[0] = rs.getString(3);
                
                view.getCbSatwil().addItem(ob[0]);                                      
            }
            rs.close(); 
            state.close();
        } catch (SQLException ex) {
            Logger.getLogger(SkckController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String getProvinsiId(SkckView view){
        try {
            String getSatwil = view.getCbSatwil().getSelectedItem().toString();
            String getIdProvinsi = "Select * From satwil where nama_satwil = '"+getSatwil+"'";
            Connection con = Config.configDB();
            Statement state = con.createStatement();
            ResultSet rs = state.executeQuery(getIdProvinsi);
            while(rs.next()){
                idProvinsi = rs.getString("id");
                view.getTestLabel().setText(idProvinsi);
            }
            rs.close(); 
            state.close();
        } catch (SQLException ex) {
            Logger.getLogger(SkckController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return idProvinsi;
    }
    
    public void setProvinsi(SkckView view){
        view.getCbProvinsi().removeAllItems(); 
        try {
            String getProvinsi = "Select * From provinsi where id = '"+idProvinsi+"'";
            Connection con = Config.configDB();
            Statement state = con.createStatement();
            ResultSet rs = state.executeQuery(getProvinsi);
            while(rs.next()){
                Object[] ob = new Object[1];
                ob[0] = rs.getString(2);
                
                view.getCbProvinsi().addItem(ob[0]);                                      
            }
            rs.close(); 
            state.close();
        } catch (SQLException ex) {
            Logger.getLogger(SkckController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String getKotaId(SkckView view){
        try {
            String getProvinsi = view.getCbProvinsi().getSelectedItem().toString();
            String getIdProvinsi = "Select * From provinsi where nama_provinsi = '"+getProvinsi+"'";
            Connection con = Config.configDB();
            Statement state = con.createStatement();
            ResultSet rs = state.executeQuery(getIdProvinsi);
            while(rs.next()){
                idKota = rs.getString("id");
                view.getTestLabel().setText(idKota);
            }
            rs.close(); 
            state.close();
        } catch (SQLException ex) {
            Logger.getLogger(SkckController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return idKota;
    }
    
    public void setKota(SkckView view){
        view.getCbKota().removeAllItems();
        try {
            String getKota = "Select * From kabupatenkota where provinsi_id = '"+idKota+"'";
            Connection con = Config.configDB();
            Statement state = con.createStatement();
            ResultSet rs = state.executeQuery(getKota);
            while(rs.next()){
                Object[] ob = new Object[1];
                ob[0] = rs.getString(3);
                
                view.getCbKota().addItem(ob[0]);                                      
            }
            rs.close(); 
            state.close();
        } catch (SQLException ex) {
            Logger.getLogger(SkckController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String getKecamatanId(SkckView view){
        try {
            String getKota = view.getCbKota().getSelectedItem().toString();
            String getIdKota = "Select * From kabupatenkota where namawilayah = '"+getKota+"'";
            Connection con = Config.configDB();
            Statement state = con.createStatement();
            ResultSet rs = state.executeQuery(getIdKota);
            while(rs.next()){
                idKecamatan = rs.getString("id");
                view.getTestLabel().setText(idKecamatan);
            }
            rs.close(); 
            state.close();
        } catch (SQLException ex) {
            Logger.getLogger(SkckController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return idKecamatan;
    }
    
    public void setKecamatan(SkckView view){
        view.getCbKecamatan().removeAllItems();
        try {
            String getKecamatan = "Select * From kecamatan where kabupatenkota_id = '"+idKecamatan+"'";
            Connection con = Config.configDB();
            Statement state = con.createStatement();
            ResultSet rs = state.executeQuery(getKecamatan);
            while(rs.next()){
                Object[] ob = new Object[1];
                ob[0] = rs.getString(3);
                
                view.getCbKecamatan().addItem(ob[0]);                                      
            }
            rs.close(); 
            state.close();
        } catch (SQLException ex) {
            Logger.getLogger(SkckController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String getKelurahanId(SkckView view){
        try {
            String getKecamatan = view.getCbKecamatan().getSelectedItem().toString();
            String getIdKota = "Select * From kecamatan where namakecamatan = '"+getKecamatan+"'";
            Connection con = Config.configDB();
            Statement state = con.createStatement();
            ResultSet rs = state.executeQuery(getIdKota);
            while(rs.next()){
                idKelurahan = rs.getString("id");
                view.getTestLabel().setText(idKelurahan);
            }
            rs.close(); 
            state.close();
        } catch (SQLException ex) {
            Logger.getLogger(SkckController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return idKelurahan;
    }
    
    public void setKelurahan(SkckView view){
        view.getCbKelurahan().removeAllItems();
        try {
            String getKelurahan = "Select * From kelurahan where kecamatan_id = '"+idKelurahan+"'";
            Connection con = Config.configDB();
            Statement state = con.createStatement();
            ResultSet rs = state.executeQuery(getKelurahan);
            while(rs.next()){
                Object[] ob = new Object[1];
                ob[0] = rs.getString(3);
                
                view.getCbKelurahan().addItem(ob[0]);                                      
            }
            rs.close(); 
            state.close();
        } catch (SQLException ex) {
            Logger.getLogger(SkckController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
