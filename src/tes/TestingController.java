/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tes;

/**
 *
 * @author spica
 */
public class TestingController {
    private TestingModel model;
    private TestingView view;
    String nama;
    
    public void setModel(TestingModel model){
        this.model = model;
    }
    
    public void hideField(TestingView view){
        view.getTxtKelas().setVisible(false);
        view.getTxtPlanet().setVisible(false);
        view.getBtnNextPrint().setVisible(false);
        view.getBtnNextPlanet().setVisible(false);
        view.getLabelNama().setVisible(false);
        view.getLabelKelas().setVisible(false);
        view.getLabelPlanet().setVisible(false);
    }
    
    public void getValueNama(TestingView view){
        nama = view.getTxtNama().getText();
        model.setNama(nama);
        view.getTxtNama().setVisible(false);
        view.getBtnNextKelas().setVisible(false);
        view.getTxtKelas().setVisible(true);
        view.getBtnNextPlanet().setVisible(true);
    }
    
}
